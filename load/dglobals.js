//pseudo-globals
const excuse = "\n\nThe rothe strikes!  You die!. . .\n\n",
	debugOnly = false, topebaseno = 2,
	alwaysLogout = true, std_logging = true, maxMsgs = 500;

//a few easier hooks for the ctrl-a codes
const ctrl_a = "\1";
const green = ctrl_a + "g", yellow = ctrl_a + "y", blue = ctrl_a + "b",
	white = ctrl_a + "w", red = ctrl_a + "r", cyan = ctrl_a + "c",
	magenta = ctrl_a + "m", high_intensity = ctrl_a + "h",
	normal = ctrl_a + "n";
const debugFields = ["flow_control", "message_posting", "message_scan",
	"instant_messaging", "navigation", "file_io", "misc"];
const readlnMax = 1536;
const maxnodes = 10;

var stillAlive = true;	//ask for advice on the 'right' way to do this

userSettings = null; roomSettings = {}; zappedRooms = null;
