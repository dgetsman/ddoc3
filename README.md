# DDoc3

DDoc3 (Damo's version of Doc) is to be a [Synchronet](http://synchro.net/) 
shell, meant to emulate the experience of _vDOC_ (or DOC in general),
implemented in the BBS family starting with ISCA BBS in the 80s, at
the University of Iowa.

## Goal and Mission

The general goals of this interface are to provide as little overhead,
for posting and reading messages, as possible.  I'm actually deferring a
little bit to the judgment of some others that've been utilizing this
interface for longer than I.  I prefer the old school BBS days, where
more functionality, and more options, meant more things to explore; this
goal is decidedly different, it is to implement as _little_ distraction
from message text/bases, and instant messages, to other users as possible.
As such, many Synchronet features will not be implemented until I have a
polished beta, and begin tucking them away where only people comfortable
with the interface will be able to stumble across them.

## Current State

DDoc3 is a rewrite of DDoc2, which had formerly hit a crappy beta state, 
but it is not yet ready for public release.  I'll be working on this in
future and I'll let you know when it's available to be tested.

## Checking it Out for Yourself

If you would like to see what this kind of system is all about, you can
find slightly differing versions of the DOC software operating at the
following two sites:

* [ISCA BBS](telnet://bbs.iscabbs.com/)
* [eSchwa BBS](telnet://bbs.eschwa.com/) - apparently offline; probably
  permanently

### Screenshots

I do not currently have any screenshots of the DDoc3 interface at hand,
though some should be coming shortly.  At this point, however, you can
find screenshots of the original BBS, that vDOC was formed to emulate, at
the following link [Citadel.org](https://citadel.org/screenshots.html).
Of course, this is only a modification of the text-mode screenshots, it
is not a fully featured desktop application as well.  You can find the
applicable screenshots by scrolling down on the aforementioned link to
the heading _Citadel Text Rooms_ and _Citadel Text Message_.

Please be aware that, at the very least, _eSchwa BBS_, is run by fascists.

## dDoc's Home

My own BBS (still pending implementation of this DOCish interface [which
will be coming very soon]) can be found at the following addresses:

* [Tinfoil Tetrahedron BBS via telnet](telnet://tintetbb.synchro.net:8022/) - **NOTE**: you can see the latest _ddoc3_ code that I was testing on this instance by creating an account and navigating to Main Menu->Xternal programs->1 (main)->10 (_ddoc3_)
* [Tinfoil Tetrahedron BBS via SSH](ssh://tintetbb.synchro.net:8023/) - **NOTE**: see the above _NOTE_, which is applicable via ssh here, also
* [Tinfoil Tetrahedron BBS (web interface)](secure via SSH: ssh://tintetbb.synchro.net:8022/) - **NOTE**: the web interface has not yet been configured for this new instance

