/*
 * ddoc3.js
 *
 * by: Damon Getsman
 * contributing/refactoring also by: @Ntwitch (github.com)
 * started: 18aug14
 * alpha phase: 25oct14
 * beta phase: 2aug15
 * code rewrite: 10Dec19
 * finished:
 *
 * a slightly more organized attempt to emulate the DOC shell from
 * within Synchronet's SSJS libraries and functionality
 */

load("load/dglobals.js");

load("load/dmsgbase.js");
load("load/docIface.js");
load("load/dexpress.js");
load("load/dmail.js");
load("load/dperroom.js");
load("load/dperuser.js");
load("load/dpoast.js");


// --==** code entry point **==--
var preSubBoard, preFileDir, preMsgGrp, preUserSettings;
var uChoice;

//save initial conditions
docIface.util.initDdoc();

/*
 * changing this to user.curgrp isn't going to work as the user object
 * has no curgrp.  need to find out if bbs.curgrp is going to work, and
 * if not, how do we reverse lookup a group from a sub code name
 */
if (userSettings.confined && (bbs.curgrp != topebaseno) &&
	userSettings.debug.flow_control) {
	//are we already in a dystopian area?
	console.putmsg(red + "CurGrp: " + bbs.curgrp + normal + "\n" +
		"Trying a jump . . .\n");
	bbs.curgrp = topebaseno;
} else if (userSettings.confined && (bbs.curgrp != topebaseno)) {
	bbs.curgrp = topebaseno;
}

if (!debugOnly) {
	/* the main program loop */
	while (stillAlive) {
		if (userSettings.debug.flow_control) {
			console.putmsg("Got " + user.cursub + " in user.cursub\n");
		}

		//dynamic prompt
		dprompt = yellow + high_intensity + msg_area.sub[user.cursub].name +
			"> ";

		//maintenance
		bbs.main_cmds++;

		//check for async messages waiting
		bbs.nodesync();

		console.putmsg("\n" + dprompt);
		uchoice = docIface.getChoice();

		switch (uchoice) {
			//top menu
			case '?':
				docIface.doMainMenu();
				break;
			//message base entry commands
			case ' ':
				uchoice = 'n';
			case 'b':
			case 'e':
			case 'E':
			case 'r':
			case 'n':
			case 'o':
			case 'k':
			case '-':
			case '%':
				msg_base.entry_level.handler(uchoice);
				break;
			//other msg base shit
			case 'j':
				//jump to new sub-board (room in DOCspeak)
				try {
					docIface.nav.jump();
				} catch (e) {
					console.putmsg(red + "Error in jump()\n" + e.message +
						"\t#: " + e.number + "\n");
				}

				//is this room zapped?
				if (userSettings.debug.navigation) {
					console.putmsg(yellow + "Testing for zapped status on" +
						" room no: " + bbs.cursub + "\n");
				}
				if (roomData.tieIns.isZapped(bbs.cursub)) {
					if (userSettings.debug.navigation) {
						console.putmsg(red + "zRooms contents: " +
							zappedRooms[user.number].zRooms + "\n");
						console.putmsg(yellow + "Attempting to unZap() " +
							bbs.cursub + "\n");
					}
					//unzap it
					roomData.tieIns.unzapRoom(bbs.cursub);
					if (userSettings.debug.navigation) {
						console.putmsg(red + "zRooms contents: " +
							zappedRooms[user.number].zRooms + "\n");
					}
				}
				break;
			//logout
			case 'l':
				console.putmsg(yellow + high_intensity + "Logout: \n");
				if (!console.noyes("Are you sure? ")) {
					docIface.util.quitDdoc();
					stillAlive = false;
				} else {
					console.putmsg(green + high_intensity +
						"Good choice.  ;)\n");
				}
				break;
			case 'w':
				wholist.list_long(wholist.populate());
				break;
			case 'x':
				express.sendX();
				break;
			case 'W':
				wholist.list_short(wholist.populate());
				break;
			case 'y':
				poast.yell();
				break;
			case 'N':
				bbs.select_shell();
				/*
				 * please note that after my torpor finishes and I get
				 * back on this tomorrow that this also requires a call
				 * to resetting the defaults/logout procedure for dDOC,
				 * and then finally (perhaps alternate way to dump the
				 * active shell) and then to respawn the user's newly
				 * selected shell; talk to the dues on Synchronet IRC
				 * about the best way to handle this or look in the 
				 * classic shell code
				 */
				docIface.util.quitDdoc();
				stillAlive = false;
				break;
			case 's':
				console.putmsg(green + high_intensity + "Skip room\n");
				docIface.nav.skip();
				break;
			case 'c':
				userConfig.reConfigure();
				break;
			case 'D':
				console.putmsg(green + high_intensity + "Change doing field\n");
				userSettings.doing = userRecords.userDataUI.getDoing();
				try {
					userRecords.userDataIO.saveSettings(user.number,
						userSettings);
					// next line will not execute if there is an exception
					// while saving
					console.putmsg(green + high_intensity + "Doing field " +
						"updated.\n\n");
				} catch (e) {
					console.putmsg(red + "Exception saving settings: "
						+ e.toString() + "\n");
				}
				break;
			case 'i':	//display room info
				roomData.roomSettingsUX.displayRoomInfo();
				break;
			case 'I':	//change room info (if applicable)
				roomData.roomSettingsUX.promptUserForRoomInfo();
				break;
			case 'z':	//zap room
				if (console.yesno("Are you sure you want to forget this " +
					"forum? ")) {
					this.log_str_n_char("z", "Trying to forget " +
						bbs.cursub_code);
					roomData.tieIns.zapRoom(bbs.cursub);
				}
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':   //we're going to read message by number here
				//there will, of course, have to be some error checking for
				//trying to hit out of bounds messages if dispMsg() doesn't
				//already have it; which I forgot for a long time, of course,
				//and now is resulting in the bug that I'm trying to fix
				try {
					msg_base.entry_level.gotoMessageByNum(uchoice);
				} catch (e) {
					console.putmsg(red + "Caught:\n" + high_intensity +
						e.name + "\n" + e.message + "\n\n");
				}

				break;
			case '$':       //change debugging flags for this user
				var dropOut = false;
				var un;

				docIface.logStatusChange("$", "Changing debugging flags",
					NODE_DFLT);

				if (user.security.level < 80) {
					userRecords.userDataUI.queryDebugSettings(user.number);
				} else {
					console.putmsg(yellow + high_intensity + "User name to " +
						"modify debug settings for: ");
					un = bbs.finduser(console.getstr());

					while (un < 1) {
						console.putmsg(red + high_intensity + "User not " +
							"found.  Enter another username or \"DONE\" to " +
							" escape.\nUsername: ");
						un = console.getstr();

						if (un == "DONE") {
							dropOut = true;
						} else {
							un = bbs.finduser(un);
						}
					}

					//we should have a valid # now or else be out of here :P
					if (!dropOut) {
						userRecords.userDataUI.queryDebugSettings(un);
					}
				}
				break;
			case 'p':       //profile a user
				var usr;
				console.putmsg(green + high_intensity +
					"User to profile -> ");

				try {
					userRecords.userDataUI.profileUser(usr = console.getstr());
					docIface.log_str_n_char("p", "Profiled " + usr);
				} catch (e) {
					console.putmsg(red + "Problem profiling: " +
						high_intensity + e.message + "\n");
				}

				break;
			default:
				console.putmsg(excuse);
				break;
		}
	}
} else {
	if (dMBTesting.init() != 0) {
		console.putmsg(red + "\n\nFUCK\n\n" + normal);
	} else {
		console.putmsg(yellow + "\nCzech and see if it's where it " +
			"should be theyah, budday\n\n" + normal);
	}
}

console.putmsg(yellow + high_intensity + "\n\nGoodbye!\n");
if (alwaysLogout) {
	bbs.logout();
}